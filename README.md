# 图片标记
* 浏览链接：http://1.15.222.141:2222/original/other/imgMark/index.html
* 项目链接：https://gitee.com/xi1213/img-mark
* csdn文章：https://blog.csdn.net/xi1213/article/details/125950643
# 项目简介
* 支持标记模式：点、直线、矩形、多边形、圆形。
* 支持自定义标记颜色。
* 支持定点缩放。
* 支持图片拖拽。
* 支持导入本地图片。
* 支持导出标记图片。
# 相关技术
* vue2主体框架。
* canvas绘制图形。
# 项目使用
* 安装依赖：npm install
* 项目运行：npm run serve
* 项目打包：npm run build
# 详情说明
![img](./mdImg/1.jpg)
* 左上角切换绘制时的颜色(可自定义颜色)。
* 右上角切换绘制模式。
* 鼠标滚轮可缩放图片大小。
* 点模式：右键点击绘制。
* 直线模式：右键拖拽绘制。
* 矩形模式：右键拖拽绘制。
* 多边形模式：右键点击绘制，左键双击自动闭合。
* 圆形模式：右键拖拽绘制。